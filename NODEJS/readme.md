
### 接口文档
0.base=http://localhost:3000

1.标签操作

1.1 添加标签

请求地址：base/tag

请求方法：POST

请求参数：

| 参数名 | 参数类型 | 必填 | 参数说明 |
| ------ | -------- | ---- | -------- |
| text | string | 是 | 标签内容 |

    示例：

```js
 {
 "text":"UnderTale2"
} 

```

请求响应：

```js
{
    status:200, //200代表成功,其他代表失败
    data:[],//请求成功后,返回的数据
    msg:""//请求失败之后，返回的数据
}
```

1.2查询标签

请求地址：base/tag

请求方法：GET

请求参数

|无|

请求响应：
```js
    {
      status:200, //200代表成功,其他代表失败
    data:[],//请求成功后,返回的数据
    msg:""//请求失败之后，返回的数据  
    }
```

1.3删除标签

请求地址：base/tag

请求方法：DELETE

请求参数

| 参数名 | 参数类型 | 必填 | 参数说明 |
| ------ | -------- | ---- | -------- |
| _id | string | 是 | id名 |

    示例：

```js
 {
 "_id":"61bc2cd28d91cdaab62181a6"
} 

```

请求响应：
```js
    {
      status:200, //200代表成功,其他代表失败
    data:[],//请求成功后,返回的数据
    msg:""//请求失败之后，返回的数据  
    }
```

1.4修改标签

请求地址：base/tag

请求方法：PUT

请求参数

| 参数名 | 参数类型 | 必填 | 参数说明 |
| ------ | -------- | ---- | -------- |
| _id | string | 是 | id名 |

    示例：

```js
 {
 "_id":"61bc2cd28d91cdaab62181a6"
} 

```

请求响应：
```js
    {
      status:200, //200代表成功,其他代表失败
    data:[],//请求成功后,返回的数据
    msg:""//请求失败之后，返回的数据  
    }
```

