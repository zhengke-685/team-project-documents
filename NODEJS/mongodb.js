const mongoose=require("mongoose");
mongoose.connect("mongodb://127.0.0.1:27017/test")



const userSchema=new mongoose.Schema({
    username:{
        type:String,
        minlength:2,
        maxlength:[12,"用户名最多12个字符"]
    },
    password:{
        type:String,
        validate:{
            validator:function(v){//验证
                return /[a-zA-Z0-9]{6,12}/.test(v);
            },
             message:"密码只能是6-12位的数、字字母和下划线的任意组合"
        },
    },
    email:{
        type:String,
        validate:{
            validator:function(v){//验证
                // return /\w+@+\.\w+/.test(v);
                return true
            },
            message:"邮箱格式不正确"
        },
    },
    group:{
        type:String,
        enum:{
            values:['限制会员','新手上路','组册会员','中级会员','高级会员'],//枚举
            messageL:"{VALUE} is not supported"
        }
    }
})


const userModel=new mongoose.model("user",userSchema)
//tagModel

module.exports={userModel};
//无法同时运行