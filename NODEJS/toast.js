module.exports={
    success:function(ctx,data=null){
        ctx.body={
            status:200,
            data:data,
            msg:""
        }
    },
    fail:function(ctx,msg){
        ctx.body={
            status:0,
            data:null,
            msg: msg.message || msg
        }
    }//,
// toast:async fun=>{
//     try{
//         const data=await fun;
//         return success(ctx,data);
//     }catch(error){
//         return fail(ctx,error);
//     }
// }
}